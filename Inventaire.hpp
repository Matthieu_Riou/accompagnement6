#ifndef INVENTAIRE_HPP
#define INVENTAIRE_HPP

#include <string>
/* Attention : dans le fichier .hpp je n'utilise pas la ligne "using namespace std;"
   Je dois alors écrire std::string à chaque fois
   Si vous mettez la ligne "using namespace std;" dans vos fichiers c++, vous pouvez juste écrire string partour
*/


// Etape 1 : objet
std::string* creerObjet(std::string nom);
void afficherObjet(std::string* obj);
void detruireObjet(std::string* obj);


// Etape 2 : inventaire
std::string** creerInventaire(int taille);
void afficherInventaire(std::string** inv, int taille);
void detruireInventaire(std::string** inv, int taille);

std::string* getObjet(std::string** inv, int taille, int num_obj);
void ramasserObjet(std::string** inv, int taille, std::string* obj);
void jeterObjet(std::string** inv, int taille, std::string* obj);
void jeterTousObjet(std::string** inv, int taille);


// Etape 3 : plusieurs inventaires
void transfererObjet(std::string** inv_source, int taille_source, std::string** inv_dest, int taille_dest, std::string* obj);
void transfererTousObjet(std::string** inv_source, int taille_source, std::string** inv_dest, int taille_dest);


#endif
